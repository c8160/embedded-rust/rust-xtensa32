FROM registry.fedoraproject.org/fedora:37

ENV NAME=rust-xtensa32 VERSION=0 ARCH=x86_64
LABEL   name="$NAME" \
        version="$VERSION" \
        architecture="$ARCH" \
        run="podman run --rm -it -v 'cargo-registry:/usr/local/cargo/registry' -v \"$PWD:/project:z\" IMAGE [cargo|rust] ..." \
        summary="Rust toolchain for embedded development (ESP32, ESP32S2)" \
        maintainer="Andreas Hartmann <hartan@7x.de>" \
        url="https://gitlab.com/c8160/embedded-rust/rust-xtensa32"

# Add documentation
COPY help.md /
COPY entrypoint.sh /

WORKDIR /root
ENV RUSTUP_HOME="/usr/share/rustup"
ENV CARGO_HOME="/usr/share/cargo"
RUN dnf install -y openssl1.1 xz gcc; \
    curl -Lo install-rust-toolchain.sh https://github.com/esp-rs/rust-build/releases/download/v1.66.0.0/install-rust-toolchain.sh; \
    chmod +x install-rust-toolchain.sh; \
    ./install-rust-toolchain.sh -b esp32 -r /root/ -s "" -e "" -i install -x YES --cargo-home "/usr/share/cargo" --rustup-home "/usr/share/rustup";
RUN source ${CARGO_HOME}/env; \
    rustup default esp; \
    rustup toolchain uninstall stable-x86_64-unknown-linux-gnu nightly-x86_64-unknown-linux-gnu; \
    rm -rf /var/cache/dnf
# Install rust-analyzer
RUN curl -L https://github.com/rust-lang/rust-analyzer/releases/latest/download/rust-analyzer-x86_64-unknown-linux-gnu.gz | gunzip -c - > /usr/local/bin/rust-analyzer; \
    chmod +x /usr/local/bin/rust-analyzer

ENV PATH="/root/.espressif/tools/xtensa-esp32-elf-clang/esp-15.0.0-20221201-x86_64-unknown-linux-gnu/bin/:/root/.espressif/tools/xtensa-esp32-elf-gcc/8_4_0-esp-2021r2-patch3-x86_64-unknown-linux-gnu/bin/:$PATH"
ENV LIBCLANG_PATH="/root/.espressif/tools/xtensa-esp32-elf-clang/esp-15.0.0-20221201-x86_64-unknown-linux-gnu/lib/"
ENV PIP_USER="no"

RUN rm -rf /usr/local/share/cargo/registry

WORKDIR /project
VOLUME /project
VOLUME /usr/local/cargo/registry
ENV CARGO_HOME="/usr/local/share/cargo"

# We specify no entrypoint here, because we leave this to the user depending on
# the tool they need. We do however call bash with the '-i' flag so it loads
# its profiles and sources e.g. the cargo env etc.
ENTRYPOINT [ "/entrypoint.sh" ]
